#======================================#
# TCL script for a mini regression     #
#======================================#

onbreak resume
onerror resume

# set environment variables
setenv DUT_SRC ./mcdf/v0
setenv TB_SRC .

# clean the environment and remove trash files
set delfiles [glob work *.log *.ucdb sim.list]

file delete -force {*}$delfiles

# compile the design and dut with a filelist
vlib work
vlog -sv -cover bst -timescale=1ps/1ps -l comp.log +incdir+$env(DUT_SRC) -f filelist.f

# prepare simrun folder
set timetag [clock format [clock seconds] -format "%Y%b%d-%H_%M"]
file mkdir regr_ucdb_${timetag}

# simulate with specific testname sequentially
set TestSets { {mcdf_data_consistence_basic_test 1} \
                {mcdf_full_random_test 3} \
                {mcdf_reg_read_write_test 1} \
                {mcdf_reg_stability_test 1} \ 
                {mcdf_down_stream_low_bandwidth_test 3} \
              }

foreach testset $TestSets {
  set testname [lindex $testset 0]
  set LoopNum [lindex $testset 1]
  for {set loop 0} {$loop < $LoopNum} {incr loop} {
    set seed [expr int(rand() * 100)]
    echo simulating $testname
    echo $seed +TESTNAME=$testname -l regr_ucdb_${timetag}/run_${testname}_${seed}.log
    vsim -onfinish stop -cover -cvgperinstance -cvgmergeinstances -sv_seed $seed \
         +TESTNAME=$testname -l regr_ucdb_${timetag}/run_${testname}_${seed}.log work.tb
    run -all
    coverage save regr_ucdb_${timetag}/${testname}_${seed}.ucdb
    quit -sim
  }
}

# merge the ucdb per test
vcover merge -testassociated regr_ucdb_${timetag}/regr_${timetag}.ucdb testplan.ucdb {*}[glob regr_ucdb_${timetag}/*.ucdb]

quit -f

